package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {

	public static final int INIT = 1000;
	public static final int FINAL = 2000;
	public static final int ELEMS = 100000;
	public static final int TO_MS = 1000000;
	
	
    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	
    	final ArrayList<Integer> list = new ArrayList<>();
    	
    	for(int j=INIT; j <= FINAL; j++) {
    		list.add(j);
    	}
    	
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	
    	final LinkedList<Integer> linkedlist = new LinkedList<>(list);
    	
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	
    	final int temp = list.get(0);
    	
    	list.set(0, list.get(list.size()-1));
    	
    	list.set(list.size()-1, temp);
    	
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	
    	for(int i: list) {
    		System.out.println(i);
    	}
    	
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	
    	long time = System.nanoTime();
    	
    	for(int i=0; i<ELEMS; i++) {
    		list.add(0, i);
    	}
    	
    	 time = System.nanoTime() - time;
         System.out.println("Inserting " + ELEMS
                 + " int in an Array List " + time
                 + "ns (" + time / TO_MS + "ms)");
         
         time = System.nanoTime();
         
         for(int i=0; i<ELEMS; i++) {
     		linkedlist.add(0, i);
     	}
         
         time = System.nanoTime() - time;
         
          System.out.println("Inserting " + ELEMS
                  + " int in a Linked List " + time
                  + "ns (" + time / TO_MS + "ms)");
    	
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
         
         time = System.nanoTime();
          
         final int middlElementFirst = list.get(list.size()/2-1);
         
         time = System.nanoTime() - time;
         
         System.out.println("Getting " + ELEMS
                 + " int from an Array List " + time
                 + "ns (" + time / TO_MS + "ms)");
         
         time = System.nanoTime();
         
         final int middlElementSecond = linkedlist.get(list.size()/2-1);
         
         time = System.nanoTime() - time;
         
         System.out.println("Getting " + ELEMS
                 + " int from a Linked List " + time
                 + "ns (" + time / TO_MS + "ms)");
         
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
         
         final Map<String, Long> world = new HashMap<>();
         
         world.put("Africa", 1_110_635_000L);
         world.put("Americas", 972_005_000L);
         world.put("Antartica", 0L);
         world.put("Asia", 4_298_723_000L);
         world.put("Europe",742_452_000L);
         world.put("Oceania", 38_304_000L);
         
         long sum = 0;
         
         for (long j : world.values()) {
        	 sum += j;         
         }
         
        /*
         * 8) Compute the population of the world
         */
         
         System.out.println("World's People is " + sum);
    }
}
